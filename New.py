    #!/usr/bin/python
""" cherrypy_example.py

    COMPSYS302 - Software Design
    Author: Andrew Chen (andrew.chen@auckland.ac.nz)
    Last Edited: 19/02/2018

    This program uses the CherryPy web server (from www.cherrypy.org).
"""
# Requires:  CherryPy 3.2.2  (www.cherrypy.org)
#            Python  (We use 2.7)

# The address we listen for connections on
listen_ip = "0.0.0.0"
listen_port = 10002

import cherrypy
import hashlib
import urllib2
import sqlite3
import json
import time
import socket
import base64
import mimetypes
import os
import os.path
import threading
import atexit

openUser = ""
loggedIn = 0
user_id = ""
user_password = ""

class MainApp(object):

    #CherryPy Configuration
    _cp_config = {'tools.encode.on': True, 
                  'tools.encode.encoding': 'utf-8',
                  'tools.sessions.on' : 'True',
                 }                 

    # If they try somewhere we don't know, catch it here and send them to the right place.

    def __init__(self):
        cherrypy.engine.subscribe("stop", self.logoutOnExit)

    @cherrypy.expose
    def default(self, *args, **kwargs):
        """The default page, given when we don't recognise where the request is for."""
        Page = "I don't know where you're trying to go, so have a 404 Error."
        cherrypy.response.status = 404
        return Page

    # PAGES (which return HTML that can be viewed in browser)
    @cherrypy.expose
    def index(self):
        Page = "Welcome! This is a test website for COMPSYS302!<br/>"
        
        try:
            # Page += "Hello " + cherrypy.session['username'] + "!<br/>"
            # Page += "Here is some bonus text because you've logged in!<br/>"
            # Page += "Click here to <a href = 'usersOnline'>check online users.<a/><br/>"
            # Page += '<form action = "/sendMessage" method = "post" enctype="multipart/form-data">'
            # Page += 'To : <input type = "text" name = "destination"/><br/>'
            # Page += 'Message : <input type = "text" name = "message"/>'
            # Page += '<input type = "submit" value = "Send"/></form>'
            # Page += '<form action="/messages" method="post" enctype="multipart/form-data">'
            # Page += '<input type = "submit" value = "Check Messages"/></form>'
            # Page += '<form action="/retrieveProfile" method="post" enctype="multipart/form-data">'
            # Page += '<input type = "text" name = "prof"/>'
            # Page += '<input type = "submit" value = "Check Profile"/></form>'
            # Page += '<form action="/updateProfile" method="post" >'
            # Page += 'Name : <input type = "text" name = "fullname"/>'
            # Page += 'Position : <input type = "text" name = "position"/>'
            # Page += 'Description : <input type = "text" name = "description"/>'
            # Page += 'Location : <input type = "text" name = "location"/>'
            # Page += 'Picture : <input type = "file" name = "profile"/>'
            # Page += '<input type = "submit" value = "Update Profile"/></form>'
            # Page += '<form action="/sendFile" method="post" enctype="multipart/form=data">'
            # Page += 'To : <input type = "text" name = "destination"/><br/>'
            # Page += 'File : <input type = "file" name = "file"/>'
            # Page += '<input type = "submit" value = "Send"/></form>'
            # Page += '<form action="/logout" method="post" enctype="multipart/form-data">'
            # Page += '<input type ="submit" value = "Logout"/></form>'
            return self.loadMainPage()

        except KeyError: #There is no username
            Page += "Click here to <a href='login'>login</a>."
        return Page

    @cherrypy.expose
    def loadMainPage(self):
        conn = sqlite3.connect("profiles.db")
        c = conn.cursor()
        c.execute('''SELECT fullname, position, description, location, picture FROM profiles WHERE username = ?''',
                  (cherrypy.session['username'],))
        rows = c.fetchall()
        print rows
        self.usersOnline()
        return open('html/mainpage.html').read().format(username=self.makeUserList(), fullname=rows[0][0],
                                                        position=rows[0][1], description=rows[0][2],
                                                        location=rows[0][3], picture=rows[0][4])

    def makeUserList(self):
        users = json.loads(self.getList())
        userlist = '<form action="/loadMessages" method="post">'
        for i in users:
            userlist += '<button name="username" value="' + users[i]["username"] + '">' + users[i]["username"] + "</button><br/>"
        userlist += '</form>'
        return userlist

    @cherrypy.expose
    def loadUpdateProfile(self):
        return open('html/updateprofile.html').read().format(username=self.makeUserList())

    @cherrypy.expose
    def loadMessages(self, username=None):
        global openUser
        openUser = username
        users = json.loads(self.getList())
        messageList = ""
        conn = sqlite3.connect("messages.db")
        c = conn.cursor()
        try:
            c.execute(
                '''CREATE TABLE messages(id INTEGER PRIMARY KEY, sender TEXT, destination TEXT, message TEXT, stamp TEXT, file INTEGER, filetype TEXT)''')
        except sqlite3.OperationalError:
            pass
        if openUser == cherrypy.session['username']:
            c.execute(
                '''SELECT sender, destination, message, stamp, file, filetype FROM messages WHERE sender = ? AND destination = ?''',
                (cherrypy.session['username'],cherrypy.session['username']))
            rows = c.fetchall()
            print rows
            for i in rows:
                if i[4] == 0:
                    messageList += '<h1 class = "mine">' + i[2] + "<h1/><br/>"
                else:
                    if i[4] == 1:
                        if i[5].find("image") != -1:
                            messageList += '<img class = "mine" width="20%" src ="' + i[2] + '" alt = "Received File"><img/>'
                        elif i[5].find("video") != -1:
                            messageList += '<video class = "mine" width="20%" controls src="' + i[2] + '"><video/>'
                        elif i[5].find("audio") != -1:
                            messageList += '<audio class = "mine" controls src="' + i[2] + '"><audio/>'
                        elif i[5].find("application") != -1:
                            messageList += '<a class = "mine pdf" ' + "href = '" + i[2] + "'>" + i[2] + "<a/>"
        else:
            for i in users:
                if users[i]["username"] == username:
                    c.execute('''SELECT sender, destination, message, stamp, file, filetype FROM messages WHERE sender = ? OR destination = ?''', (username, username))
                    rows = c.fetchall()
                    print rows
                    for i in rows:
                        if i[0] == cherrypy.session['username']:
                            if i[4] == 0:
                                messageList += '<h1 class = "mine">' + i[2] + "<h1/><br/>"
                            else:
                                if i[5].find("image") != -1:
                                    messageList += '<img class = "mine" src ="' + i[2] + '" alt = "Received File"><img/><br/>'
                                elif i[5].find("video") != -1:
                                    messageList += '<video class = "mine" width="300" height = "200" controls src="' + i[2] + '"><video/><br/>'
                                elif i[5].find("audio") != -1:
                                    messageList += '<audio class = "mine" controls src="' + i[2] + '"><audio/><br/>'
                                elif i[5].find("application") != -1:
                                    messageList += '<a class = "mine pdf" ' + "href = '" + i[2] + "'>" + i[2] + "<a/><br/>"
                        else:
                            if i[4] == 0:
                                messageList += '<h1 class = "theirs">' + i[2] + "<h1/><br/>"
                            else:
                                if i[4] == 1:
                                    if i[5].find("image") != -1:
                                        messageList += '<img class = "theirs" src ="' + i[2] + '" alt = "Received File"><img/><br/>'
                                    elif i[5].find("video") != -1:
                                        messageList += '<video class = "theirs" width="300" height = "200" controls src="' + i[2] + '"><video/><br/>'
                                    elif i[5].find("audio") != -1:
                                        messageList += '<audio class = "theirs" controls src="' + i[2] + '"><audio/><br/>'
                                    elif i[5].find("application") != -1:
                                        messageList += '<a class = "mine pdf" ' + "href = '" + i[2] + "'>" + i[2] + "<a/><br/>"
                    print messageList
        self.retrieveProfile(openUser)
        conn = sqlite3.connect("profiles.db")
        c = conn.cursor()
        c.execute('''SELECT fullname, position, description, location, picture FROM profiles WHERE username = ?''', (openUser,))
        rows = c.fetchall()
        return open('html/messages.html').read().format(username=self.makeUserList(), messages=messageList,
                                                        fullname=rows[0][0], position=rows[0][1],
                                                        description=rows[0][2], location=rows[0][3],
                                                        picture=rows[0][4])

    @cherrypy.expose
    def loadOnlineUsers(self):
        users = json.loads(self.getList())
        list = ""
        for i in users:
            list += "<button>" + i + "</button>"
        open('html/mainpage.html').read().format(username = list)

    @cherrypy.expose
    def login(self):

        return open('html/1.html').read()
    
        # Page = '<form action="/signin" method="post" enctype="multipart/form-data">'
        # Page += 'Username: <input type="text" name="username"/><br/>'
        # Page += 'Password: <input type="password" name="password"/>'
        # Page += '<input type="submit" value="Poos"/></form>'
        # return Page

    @cherrypy.expose
    def loggedoff(self):
        Page = "You have successfully logged off!<br/>"
        Page += "Click here to <a href = 'login'>login again</a>."
        return Page

    @cherrypy.expose
    def logofferror(self):
        Page = 'Something went wrong! You may have not been logged in.<br/>'
        Page += "Click here to <a href = 'login'>return to the login page</a>."
        return Page

    @cherrypy.expose
    def messages(self):
        Page = 'Messages:<br/><br/>'
        conn = sqlite3.connect("messages.db")
        c = conn.cursor()
        try:
            c.execute('''CREATE TABLE messages(id INTEGER PRIMARY KEY, sender TEXT, destination TEXT, message TEXT, stamp TEXT, file INTEGER, filetype TEXT)''')
        except sqlite3.OperationalError:
            pass
        c.execute('''SELECT * FROM messages''')

        rows = c.fetchall()
        print rows
        for i in rows:
            Page += "Sender : " + str(i[1]) + "<br/>"
            Page += "Destination : " + str(i[2]) + "<br/>"
            Page += "Message : " + str(i[3]) + "<br/>"
            Page += "Stamp : " + str(i[4]) + "<br/>" + "<br/>"
        conn.close()
        return Page

    @cherrypy.expose
    def sum(self, a=0, b=0): #All inputs are strings by default
        output = int(a)+int(b)
        return str(output)

    @cherrypy.expose
    def ping(self, sender):
        return '0'

    @cherrypy.expose
    def getList(self):
        username = cherrypy.session['username']
        password = cherrypy.session['password']
        res = urllib2.urlopen("http://cs302.pythonanywhere.com/getList?username=" + username
                              + "&password=" + password
                              + "&json=1").read()
        return res

    @cherrypy.tools.json_in()
    @cherrypy.expose
    def receiveMessage(self):
        received = cherrypy.request.json
        conn = sqlite3.connect("messages.db")
        c = conn.cursor()
        try:
            c.execute('''CREATE TABLE messages(id INTEGER PRIMARY KEY, sender TEXT, destination TEXT, message TEXT, stamp TEXT, file INTEGER, filetype TEXT)''')
        except sqlite3.OperationalError:
            pass
        if received["sender"] == received["destination"]:
            pass
        else:
            c.execute('''INSERT INTO messages(sender, destination, message, stamp, file, filetype) VALUES(?,?,?,?,?,?)''', (received["sender"], received["destination"], received["message"], received["stamp"], 0, ""))
            conn.commit()
        conn.close()
        return '0'

    @cherrypy.expose
    def sendMessage(self, message = None):
        username = cherrypy.session['username']
        users = json.loads(self.getList())
        # url = "http://" + users[i]["ip"] + ":" + users[i]["port"] + "/ping?sender=" + username
        # if urllib2.urlopen(url).read() == '0':
        conn = sqlite3.connect("messages.db")
        c = conn.cursor()
        try:
            c.execute('''CREATE TABLE messages(id INTEGER PRIMARY KEY, sender TEXT, destination TEXT, message TEXT, stamp TEXT, file INTEGER, filetype TEXT)''')
        except sqlite3.OperationalError:
            pass
        for i in users:
            if users[i]["username"] == openUser and urllib2.urlopen("http://" + users[i]["ip"] + ":" + users[i]["port"] + "/ping?sender=" + username).read() == '0':
                url = "http://" + users[i]["ip"] + ":" + users[i]["port"] + "/receiveMessage"
                mes = { "sender" : cherrypy.session['username'], "destination" : openUser, "message" : message, "stamp" : time.time()}
                data = json.dumps(mes)
                req = urllib2.Request(url, data, {'Content-Type' : 'application/json'})
                urllib2.urlopen(req)
                c.execute('''INSERT INTO messages(sender, destination, message, stamp, file, filetype) VALUES(?,?,?,?,?,?)''', (username, openUser, message, time.time(), 0, "None"))
                conn.commit()
        conn.close()
        return self.loadMessages(openUser)
        # raise cherrypy.HTTPRedirect(/loadMessages?openUser)

    @cherrypy.expose
    def usersOnline(self):
        res = json.loads(self.getList())
        conn = sqlite3.connect("list.db")
        c = conn.cursor()
        try:
            c.execute('''CREATE TABLE users(id INTEGER PRIMARY KEY, username TEXT, ip TEXT, location TEXT, lastLogin TEXT, port TEXT)''')
        except sqlite3.OperationalError:
            pass
        c.execute('''DELETE FROM users''')
        for i in res:
            c.execute('''INSERT INTO users(username, ip, location, lastLogin, port) VALUES(?,?,?,?,?)''', (res[i]["username"],res[i]["ip"],res[i]["location"],res[i]["lastLogin"],res[i]["port"]))
        conn.commit()
        Page = ''
        c.execute('''SELECT * FROM users''')
        rows = c.fetchall()
        for row in rows:
            Page += "Username : " + str(row[1]) + "<br/>"
            Page += "IP : " + str(row[2]) + "<br/>"
            Page += "Location :" + str(row[3]) + "<br/>"
            Page += "Last Login : " + str(row[4]) + "<br/>"
            Page += "Port : " + str(row[5]) + "<br/>" + "<br/>"
        conn.close()
        return Page

    @cherrypy.expose
    def retrieveProfile(self, prof = None):
        users = json.loads(self.getList())
        conn = sqlite3.connect("profiles.db")
        c = conn.cursor()
        try:
            c.execute('''CREATE TABLE profiles(username TEXT PRIMARY KEY, lastUpdated TEXT, fullname TEXT, position TEXT, description TEXT, location TEXT, picture TEXT, encoding TEXT, encryption TEXT, decryptionKey TEXT)''')
        except sqlite3.OperationalError:
            pass
        for i in users:
            if users[i]["username"] == prof:
                url = 'http://' + users[i]["ip"] + ":" + users[i]["port"] + "/getProfile"
                data = json.dumps({"profile_username" : prof, "sender" : cherrypy.session['username']})
                req = urllib2.Request(url, data, {'Content-Type' : 'application/json'})
                received = json.loads(urllib2.urlopen(req).read())
                try:
                    c.execute('''INSERT INTO profiles(username, lastUpdated, fullname, position, description, location, picture) VALUES(?,?,?,?,?,?,?)''', (prof, received["lastUpdated"], received["fullname"], received["position"], received["description"], received["location"], received["picture"]))
                except sqlite3.IntegrityError:
                    c.execute('''UPDATE profiles SET lastUpdated = ?, fullname = ?, position = ?, description = ?, location = ?, picture = ? WHERE username = ?''', (received["lastUpdated"], received["fullname"], received["position"], received["description"], received["location"], received["picture"], prof))
                except KeyError:
                    print "Error! Profile not valid"
                    raise cherrypy.HTTPRedirect('/')
                conn.commit()
        conn.close()

    @cherrypy.tools.json_in()
    @cherrypy.expose
    def getProfile(self):
        received = cherrypy.request.json
        conn = sqlite3.connect("profiles.db")
        c = conn.cursor()
        c.execute('''SELECT lastUpdated, fullname, position, description, location, picture FROM profiles WHERE username = ?''', (received['profile_username'],))
        rows = c.fetchall()
        profile = {"lastUpdated" : str(rows[0][0]), "fullname" : str(rows[0][1]), "position" : str(rows[0][2]), "description" : str(rows[0][3]), "location" : str(rows[0][4]), "picture" : str(rows[0][5])}
        print profile
        data = json.dumps(profile)
        return data

    @cherrypy.expose
    def createInitialProfile(self):
        conn = sqlite3.connect("profiles.db")
        c = conn.cursor()
        try:
            c.execute('''CREATE TABLE profiles(username TEXT PRIMARY KEY, lastUpdated TEXT, fullname TEXT, position TEXT, description TEXT, location TEXT, picture TEXT, encoding TEXT, encryption TEXT, decryptionKey TEXT)''')
        except sqlite3.OperationalError:
            print "1"
            pass
        try:
            c.execute(
                '''INSERT INTO profiles(username, lastUpdated, fullname, position, description, location, picture) VALUES(?,?,?,?,?,?,?)''',
                (cherrypy.session['username'], time.time(), "(No Name)", "", "", "", "static/whitesquare.jpg"))
        except sqlite3.IntegrityError:
            pass
        conn.commit()
        conn.close()

    @cherrypy.expose
    def updateProfile(self, fullname = None, position = None, description = None, location = None, profile = None):
        username = cherrypy.session['username']
        conn = sqlite3.connect("list.db")
        c = conn.cursor()
        c.execute('''SELECT ip, port FROM users WHERE username = ?''', (username,))
        rows = c.fetchall()
        conn = sqlite3.connect("profiles.db")
        c = conn.cursor()
        try:
            f = open(profile, "r")
            pic = f.name
        except IOError:
            pic = "whitesquare.jpg"
            print pic
        if fullname == "":
            fullname = "(No Name)"
        print fullname
        try:
            c.execute('''CREATE TABLE profiles(username TEXT PRIMARY KEY, username TEXT, lastUpdated TEXT, fullname TEXT, position TEXT, description TEXT, location TEXT, picture TEXT, encoding TEXT, encryption TEXT, decryptionKey TEXT)''')
        except sqlite3.OperationalError:
            pass
        c.execute('''UPDATE profiles SET lastUpdated = ?, fullname = ?, position = ?, description = ?, location = ?, picture = ? WHERE username = ?''',
                  (time.time(), fullname, position, description, location, "http://" + rows[0][0] + ":" + rows[0][1] + "/static/" + pic, "ilee471"))
        conn.commit()
        conn.close()
        raise cherrypy.HTTPRedirect('/')

    @cherrypy.tools.json_in()
    @cherrypy.expose
    def receiveFile(self):
        received = cherrypy.request.json
        decoded = base64.decodestring(received["file"])
        cwd = os.getcwd()
        os.chdir(cwd+"/files")
        res = open(received["filename"], "wb")
        res.write(decoded)
        res.close()
        os.chdir(cwd)
        conn = sqlite3.connect("messages.db")
        c = conn.cursor()
        print received["content_type"]
        try:
            c.execute('''CREATE TABLE messages(id INTEGER PRIMARY KEY, sender TEXT, destination TEXT, message TEXT, stamp TEXT, file INTEGER, filetype TEXT)''')
        except sqlite3.OperationalError:
            pass
        c.execute('''INSERT INTO messages(sender, destination, message, stamp, file, filetype) VALUES (?,?,?,?,?,?)''',
                (received["sender"], received["destination"], "static/files/" + received["filename"], received["stamp"],
                   1, received["content_type"]))
        conn.commit()
        conn.close()
        return '0'

    @cherrypy.expose
    def sendFile(self, destination = None, file = None):
        destination = openUser
        # if destination is None or file is None:
        #     raise cherrypy.HTTPRedirect('/')
        users = json.loads(self.getList())
        for i in users:
            if users[i]["username"] == destination and urllib2.urlopen("http://" + users[i]["ip"] + ":" + users[i]["port"] + "/ping?sender=" + cherrypy.session['username']).read() == '0':
                r = file.file.read()
                file_data = base64.encodestring(r)  # base 64 encoding of file data
                filename = file.filename
                cwd = os.getcwd()
                os.chdir(cwd + "/files")
                save = open(filename, "wb")
                save.write(r)
                save.close()
                os.chdir(cwd)
                if os.path.getsize("files/" + filename) < 5000000:
                    print filename
                    content_type = str(file.content_type)
                    conn = sqlite3.connect("messages.db")
                    c = conn.cursor()
                    try:
                        c.execute(
                            '''CREATE TABLE messages(id INTEGER PRIMARY KEY, sender TEXT, destination TEXT, message TEXT, stamp TEXT, file INTEGER, filetype TEXT)''')
                    except sqlite3.OperationalError:
                        pass
                    c.execute(
                        '''INSERT INTO messages(sender, destination, message, stamp, file, filetype) VALUES (?,?,?,?,?,?)''',
                        (user_id, destination, "static/files/" + filename, time.time(), 1, content_type))
                    conn.commit()
                    conn.close()
                    json_data = {"sender": cherrypy.session['username'], "destination": destination, "file": file_data,
                                 "filename": filename, "content_type": content_type, "stamp": time.time()}
                    data = json.dumps(json_data)
                    url = "http://" + users[i]["ip"] + ":" + users[i]["port"] + "/receiveFile"
                    req = urllib2.Request(url, data, {'Content-Type' : 'application/json'})
                    urllib2.urlopen(req)
        return self.loadMessages(openUser)

    # LOGGING IN AND OUT
    @cherrypy.expose
    def signin(self, username=None, password=None):
        """Check their name and password and send them either to the main page, or back to the main login screen."""
        self.location()
        error = self.authoriseUserLogin(username,password, cherrypy.session['location'])
        if error == 0:
            cherrypy.session['username'] = username
            cherrypy.session['password'] = hashlib.sha256(password+username).hexdigest()
            global user_id
            global user_password
            user_id = cherrypy.session['username']
            user_password = cherrypy.session['password']
            self.initializeLoginThread()
            self.createInitialProfile()
            raise cherrypy.HTTPRedirect('/')
        else:
            raise cherrypy.HTTPRedirect('/login')

    @cherrypy.expose
    def logout(self):
        error = self.signout()
        if error == 0:
            raise cherrypy.HTTPRedirect('/loggedoff')
        else:
            raise cherrypy.HTTPRedirect('/logofferror')

    @cherrypy.expose
    def location(self):
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(("8.8.8.8", 80))
        ip = (s.getsockname()[0])
        s.close()
        if ip.find("10.103") == 0:
            lo = "0"
        elif ip.find("172.23") == 0:
            lo = "1"
        else:
            lo = "2"
        cherrypy.session['location'] = lo

    @cherrypy.expose
    def signout(self):
        """Logs the current user out, expires their session"""
        print user_id
        print user_password
        res = urllib2.urlopen("http://cs302.pythonanywhere.com/logoff?username=" + user_id
                                + "&password=" + user_password)
        num = res.read()
        try:
            cherrypy.lib.sessions.expire()
        except KeyError:
            pass
        print num
        if num[0] == '0':
            return 0
        else:
            return 1

    def initializeLoginThread(self):
        if cherrypy.session['username'] is None:
            raise cherrypy.HTTPRedirect('/login')
        else:
            cherrypy.session['logged'] = threading.Event()
            cherrypy.session['logged'].set()
            thread = threading.Thread(target = self.autorelogin, args=(cherrypy.session['username'], cherrypy.session['password'],
                                      cherrypy.session['logged'], cherrypy.session['location']))
            thread.daemon = True
            thread.start()

    def autorelogin(self, username = "", password = "", logged = "", location = ""):
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(("8.8.8.8", 80))
        ip = (s.getsockname()[0])
        #ip = "172.23.20.177"
        s.close()

        while logged.isSet():
            req = urllib2.Request("http://cs302.pythonanywhere.com/report?username=" + username
                            + "&password=" + password
                            + "&location=" + location
                            + "&ip=" + ip
                            + "&port=" + str(listen_port))
            num = urllib2.urlopen(req).read()
            time.sleep(60)


    def authoriseUserLogin(self, username, password, location):
        print username
        print password
        password = hashlib.sha256(password + username).hexdigest()
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(("8.8.8.8", 80))
        ip = (s.getsockname() [0])
        #ip = "172.23.20.177"
        s.close()
        res = urllib2.urlopen("http://cs302.pythonanywhere.com/report?username=" + username
                             + "&password=" + password
                             + "&location=" + location
                             + "&ip=" + ip
                             + "&port=" + str(listen_port))
        num = res.read()
        if num[0] == '0':
            global loggedIn
            loggedIn = 1
            return 0
        else:
            return 1

    @cherrypy.expose
    def logoutOnExit(self):
        if loggedIn == 1:
            print 1
            cherrypy.session['logged'].clear()
            self.signout()
          
def runMainApp():
    # Create an instance of MainApp and tell Cherrypy to send all requests under / to it. (ie all of them)
    cherrypy.tree.mount(MainApp(), "/")
    c = { '/static' : {
        'tools.staticdir.on' : True,
        'tools.staticdir.dir' : os.path.abspath(os.getcwd())
        }}
    cherrypy.tree.mount(MainApp(), "/", c)

    # Tell Cherrypy to listen for connections on the configured address and port.
    cherrypy.config.update({'server.socket_host': listen_ip,
                            'server.socket_port': listen_port,
                            'engine.autoreload.on': True,
                           })

    print "========================="
    print "University of Auckland"
    print "COMPSYS302 - Software Design Application"
    print "========================================"                       
    
    # Start the web server
    cherrypy.engine.start()

    # And stop doing anything else. Let the web server take over.
    cherrypy.engine.block()
 
#Run the function to start everything
runMainApp()
