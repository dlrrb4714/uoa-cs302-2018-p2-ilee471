# UoA-CS302-2018-P2-ilee471
Instructions on running the system:
1. Install Python and CherryPy
1. Get git clone link from bitbucket
2. Open terminal at desired directory for repository
3. Paste link and run
4. Open terminal at repository directory
5. Type "python New.py" and run
6. Open internet browser and type address "localhost:10002"

Dependencies : CherryPy

receiveMessage() / sendMessage() - Darin Choi (dcho415), Dennis Cheung(dche192)
receiveFile() / sendFile() - Darin Choi (dcho415), Dennis Cheung(dche192)
retrieveProfile() / getProfile() - Darin Choi (dcho415), Dennis Cheung(dche192)